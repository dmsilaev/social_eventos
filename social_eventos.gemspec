# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'social_eventos/version'

Gem::Specification.new do |spec|
  spec.name          = "social_eventos"
  spec.version       = SocialEventos::VERSION
  spec.authors       = ["Silaev Dmitry"]
  spec.email         = ["dmsilaev@yandex.ru"]
  spec.description   = %q{stream instagram and twitter}
  spec.summary       = %q{}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "hashie"
  spec.add_dependency "tweetstream"
  spec.add_dependency "em-instagram"
  spec.add_dependency "em-http-server"
  spec.add_dependency "vkontakte_api"
  spec.add_dependency 'thin'
  spec.add_dependency 'sinatra'
  spec.add_dependency 'instagram', '= 0.11.0'
  spec.add_dependency 'addressable'
  spec.add_dependency "logger"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
