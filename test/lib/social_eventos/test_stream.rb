require 'test_helper'

class TestStream < MiniTest::Test
  def setup
    SocialEventos.configure do |config|
      config.twitter = {
        consumer_key: 'bIA5tSM16ax5KFaOtu2K4Q',
        consumer_secret: '568bTM3krT9sX8q31ttm0lFrRStrrFaetU7qUkQUnw',
        oauth_token: '239023143-jutFqvytAFIlZIv51d4eqEk5beZkHFbH5O5vTh4r',
        oauth_token_secret: 'gf4SZnN2pYhkTrG31Hbq7jvIlFsYHZNAF9ck6ERj9iJ87',
        auth_method: :oauth
      }
      config.instagram = {
        client_id: '783eca61a35a4ba29cde17f5b3f7d8f4',
        client_secret: 'ac3da9d255a24070aa1a491725877ccf',
        callback_url: 'http://localhost:3000/users/auth/instagram/callback'
      }
    end

    @stream = SocialEventos::Stream.instance
    @stream.tags = ['abc','cde']
  end

  def test_new
    assert_equal @stream.tags, ['abc','cde']
  end

  def test_clients
    assert_equal @stream.clients.first.client_name, 'twitter'
    assert_equal @stream.clients.count, 2
  end

  def test_tags
    assert_equal @stream.tags, ['abc','cde']
  end

  def test_with_clients
    @stream.tags = ['abc','cde']
    @stream.clients = 'instagram'

    assert_equal @stream.clients.first.client_name, 'instagram'
    assert_equal @stream.clients.count, 1
  end

  def test_run_all
    # binding.pry
  end

end
