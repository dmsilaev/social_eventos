require 'test_helper'

class TestClient < MiniTest::Test
  class SocialEventos::TestClient < SocialEventos::Client
    class << self
      attr_accessor :test_client_variable
    end
  end

  def setup
    @client = SocialEventos::TestClient.instance
    @client.tags = ['tag']
  end

  def test_class_configure
    SocialEventos::TestClient.configure(test_client_variable: 'var')

    assert_equal SocialEventos::TestClient.test_client_variable, 'var'
  end

  def test_class_tags
    assert_equal @client.class.tags, ['tag']
  end

  def test_client_name
    assert_equal @client.client_name, 'test'
  end
end
