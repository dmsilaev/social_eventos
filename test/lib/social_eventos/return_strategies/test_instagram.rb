require 'test_helper'

class TestInstagram < MiniTest::Test
  def setup


    @api_value = '[{"changed_aspect": "media", "object": "tag", "object_id": "sport", "time": 1396960665, "subscription_id": 4567832, "data": {}}]'

    @client = SocialEventos::InstagramClient.instance
    @client.tags = ['sport']
  end

  def test_strategy
    @strategy = SocialEventos::ReturnStrategy.new(SocialEventos::InstagramReturnStrategy, "#{@api_value}")
    @object = @strategy.get_object
  end

end
