require 'test_helper'

class TestConfiguration < MiniTest::Test
  class SocialEventos::TestClient < SocialEventos::Client
    class << self
      attr_accessor :first_variable
    end
  end

  class SocialEventos::SecondTestClient < SocialEventos::Client
    class << self
      attr_accessor :second_variable
    end
  end

  def setup
    SocialEventos.configure do |config|
      config.test = { first_variable: 'first_var' }
      config.second_test =  { second_variable: 'second_var' }
    end
  end

  def test_configure_clients
    assert_equal SocialEventos::TestClient.first_variable, 'first_var'
    assert_equal SocialEventos::SecondTestClient.second_variable, 'second_var'
  end

  def test_read_configuration
    assert_equal SocialEventos.configuration.test.first_variable, 'first_var'
  end

  def test_get_clients
    assert_equal SocialEventos.configuration.configured_clients.include?('TestClient')
    assert_equal SocialEventos.configuration.configured_clients.include?('SecondTestClient')
  end
end
