require 'net/http'

class SocialEventos::Sender
  class << self
    class MissingProcessorUrlError < RuntimeError; end

    def send(object = nil)
      return unless object
      send_object_to_processor_url(object)
    end

    private

    def send_object_to_processor_url(object)
      fail MissingProcessorUrlError if SocialEventos.configuration.processor_url.nil?
      Net::HTTP.post_form(URI(SocialEventos.configuration.processor_url), object)
    end
  end
end
