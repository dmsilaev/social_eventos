require 'singleton'

class SocialEventos::Client
  include  Singleton

  attr_accessor :tags

  class << self
    def configure(args)
      args.each do |arg, val|
        send "#{arg}=", val
      end
    end
  end

  def client_name
    @client_name ||= self.class.to_s.split('::').last.downcase.gsub('client', '')
  end
end
