class SocialEventos::ReturnStrategy
  def initialize(strategy, data, tag)
    @strategy = strategy
    @data = data
    @tag = tag
  end

  def get_object
    object = @strategy.create_object(@data)
    object[:tag] = { name: @tag }
    object
  end
end
