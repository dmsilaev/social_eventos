class SocialEventos::Configuration
  attr_reader :configured_clients
  attr_accessor :processor_url

  def initialize
    @configured_clients = []
  end

  def method_missing(meth, *args, &block)
    class_name = get_client_class(meth)

    return super unless SocialEventos.const_defined?(class_name)

    add_client(class_name)

    if meth.to_s.include?('=')
      SocialEventos.const_get(class_name).configure(*args)
    else
      SocialEventos.const_get(class_name)
    end
  end

  private

  def get_client_class(method)
    (method.to_s.delete('=') + '_client').split('_').map(&:capitalize).join
  end

  def add_client(class_name)
    @configured_clients << class_name unless @configured_clients.include?(class_name)
  end
end
