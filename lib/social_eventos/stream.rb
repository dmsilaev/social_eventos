require 'eventmachine'

class SocialEventos::Stream < SocialEventos::Client
  attr_reader :clients

  def clients=(*clients)
    @clients = clients_by_name(clients) || configured_clients
  end

  def clients
    @clients ||= configured_clients
  end

  def start_all
    EM.run do
      clients.each do |client|
        EM.defer do
          client.start
        end
      end
    end
  end

  private

  def configured_clients
    SocialEventos.configuration.configured_clients.map do |class_name|
      client = SocialEventos.const_get(class_name).instance
      client.tags = tags
      client
    end
  end

  def clients_by_name(*clients)
    return if clients == []

    class_names = clients.map { |c_n| "#{c_n}_client".split('_').map(&:capitalize).join }
    class_names.map do |class_name|
      client = SocialEventos.const_get(class_name).instance
      client.tags = tags
      client
    end
  end
end
