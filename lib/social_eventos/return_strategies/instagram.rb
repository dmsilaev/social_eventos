class SocialEventos::InstagramReturnStrategy
  class << self
    def create_object(instagram_object)
      { message: {
          network_link: instagram_object.link,
          remote_image_url: instagram_object.images.standard_resolution.url,
          content: instagram_object.caption.nil? ? nil : instagram_object.caption.text,
          network: 'instagram'
        },
        user: {
          # nickname: instagram_object.user.username,
          remote_avatar_url: instagram_object.user.profile_picture,
          name: instagram_object.user.full_name
        }
      }
    end
  end
end
