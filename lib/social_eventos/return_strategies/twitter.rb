class SocialEventos::TwitterReturnStrategy
  class << self
    def create_object(twitter_object)
      tweet_image = get_tweet_image(twitter_object.media)
      content = get_tweet_content(twitter_object, tweet_image: tweet_image)
      remote_image_url = get_tweet_remote_image_url(tweet_image).to_s

      { message:
          {
            network_link: twitter_object.uri.to_s,
            remote_image_url: remote_image_url,
            content: content,
            network: 'twitter'
          },
        user:
          {
            remote_avatar_url: twitter_object.user.profile_image_url('400x400').to_s,
            name: twitter_object.user.name
          }
      }
    end

    def get_tweet_image(media = [])
      media.select { |m| m.is_a?(Twitter::Media::Photo) }.first
    end

    def get_tweet_content(object, options = {})
      tweet_image = options.delete(:tweet_image)
      return object.text if tweet_image.nil?

      object.text.gsub(tweet_image.uri.to_s, '')
    end

    def get_tweet_remote_image_url(tweet_image)
      return nil if tweet_image.nil?
      tweet_image.media_uri
    end
  end
end
