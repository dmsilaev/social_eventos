class SocialEventos::VkReturnStrategy
  class << self
    def create_object(vk_object)
      { message: {
          network_link: create_vk_link(vk_object.owner_id, vk_object.id),
          remote_image_url: get_vk_image(vk_object),
          content: vk_object.text,
          network: 'vk'
        },
        user: {
          # nickname: twitter_object.user.screen_name,
          remote_avatar_url: get_vk_user_avatar(vk_object),
          name: get_vk_user_name(vk_object)
        }
      }
    end


    private
    def create_vk_link user_id, post_id
      URI::HTTPS.build({host: 'vk.com', path: "/wall_#{user_id}_#{post_id}"}).to_s
    end

    def get_vk_image vk_object
      return nil unless (vk_object.attachments.is_a? Array)

      photo = vk_object.attachments.select { |o| o.type == 'photo' }.first
      (photo != nil) ? photo.photo.src : nil
    end

    def get_vk_user_avatar vk_object
      return vk_object.user.photo unless vk_object.user == nil
      return vk_object.group.photo unless vk_object.group == nil
    end

    def get_vk_user_name vk_object
      return "#{vk_object.user.first_name vk_object.user.last_name}" unless vk_object.user == nil
      return vk_object.group.name unless vk_object.group == nil
    end
  end

end
