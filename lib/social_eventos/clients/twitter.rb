class SocialEventos::TwitterClient < SocialEventos::Client
  class TwitterConnectionError < RuntimeError; end
  class TwitterMessageError < RuntimeError; end

  require 'tweetstream'

  class << self
    attr_accessor :consumer_key
    attr_accessor :consumer_secret
    attr_accessor :oauth_token
    attr_accessor :oauth_token_secret
    attr_accessor :auth_method
  end

  def start
    configure_client

    @tags.each do |tag|
      @twitter_client.track(tag) do |twitter_object|
        EM.run do
          EM.defer do
            SocialEventos::Sender.send(object_to_send(twitter_object, tag))
          end
        end
      end
    end
  end

  private

  def configure_client
    TweetStream.configure do |config|
      config.consumer_key       =  self.class.consumer_key
      config.consumer_secret    =  self.class.consumer_secret
      config.oauth_token        =  self.class.oauth_token
      config.oauth_token_secret =  self.class.oauth_token_secret
      config.auth_method        =  self.class.auth_method
    end

    @twitter_client = TweetStream::Client.new.on_reconnect do |timeout, retries|
      puts "#{timeout} #{retries} TweetStream::ReconnectError"
    end
  end

  def object_to_send(object, tag)
    SocialEventos::ReturnStrategy.new(SocialEventos::TwitterReturnStrategy, object, tag).get_object
  end
end
