class SocialEventos::InstagramClient < SocialEventos::Client
  require 'instagram'
  require 'addressable/uri'

  class << self
    attr_accessor :client_id
    attr_accessor :client_secret
    attr_accessor :callback_url
    attr_accessor :current_prev_id
  end

  @current_prev_id = {}

  def start
    create_subscriptions
  end

  def create_subscriptions
    EM.run do
      EM.add_periodic_timer(3) do
        @tags.each do |tag|

          tag.gsub!(/(@|#)/, '') # В апи инстаграма не передается решетка и собачка
          instagram_object = get_new_instagram_object(Addressable::URI.parse(tag).normalize.to_str)

          next unless instagram_object

          EM.defer { SocialEventos::Sender.send(object_to_send(instagram_object, tag)) }

        end
      end
    end
  end

  def instagram_client
    @instagram_client ||= Instagram.client(client_id: self.class.client_id, client_secret: self.class.client_secret)
  end

  def get_new_instagram_object(tag)
    current_prev_object_id = self.class.current_prev_id[tag]

    instagram_objects = instagram_client.tag_recent_media(tag)
    prev_object = select_prev_object(instagram_objects, current_prev_object_id)

    new_insta_object = select_new_insta_object_by_prev_object(instagram_objects, prev_object)

    return unless check_new_insta_object(new_insta_object, tag)

    self.class.current_prev_id[tag] = new_insta_object.id

    new_insta_object
  end

  private

  def select_prev_object(instagram_objects, prev_id)
    instagram_objects.select { |obj| obj.id == prev_id }.first
  end

  def select_new_insta_object_by_prev_object(instagram_objects, prev_object = nil)
    return instagram_objects.first unless prev_object

    prev_object_index = instagram_objects.find_index(prev_object)

    return nil if prev_object_index == 0

    my_insta_object_index = prev_object_index - 1
    instagram_objects[my_insta_object_index]
  end

  def check_new_insta_object(insta_object, tag)
    return nil unless insta_object
    self.class.current_prev_id[tag] != insta_object.id
  end

  def object_to_send(object, tag)
    SocialEventos::ReturnStrategy.new(SocialEventos::InstagramReturnStrategy, object, tag).get_object
  end
end
