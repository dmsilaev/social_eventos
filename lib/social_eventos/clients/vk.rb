class SocialEventos::VkClient < SocialEventos::Client
  require 'vkontakte_api'
  class VkTagError < RuntimeError; end

  def start
    create_subscriptions
  end

  def create_subscriptions
    EM.run do
      EM.add_periodic_timer(3) do
        @tags.each do |tag|
          checked_tag = check_or_create_tag tag
          vk_object = get_vk_object checked_tag
          unless vk_object.nil?
            EM.defer do
              collected_object = SocialEventos::ReturnStrategy.new(SocialEventos::VkReturnStrategy, vk_object, tag).get_object
              SocialEventos::Sender.send(collected_object)
            end
          end
        end
      end
    end
  end

  private

  def check_or_create_tag(string)
    tag = (/#.*/=~string) == 0 ? string : "##{string}"
    fail VkTagError unless /#.*/=~tag
    tag
  end

  def vk_client
    @vk_client ||= VkontakteApi::Client.new
  end

  def get_vk_object(tag)
    @@next_prev_id ||= {}
    in_objects = vk_client.newsfeed.search({ q: tag, extended: 1 })[1..-1]

    return nil unless (in_objects.is_a? Array)

    prev_object = @@next_prev_id[tag].nil? ? nil : in_objects.select { |obj| obj.id == @@next_prev_id[tag] }.first
    prev_object_index = in_objects.find_index(prev_object)

    my_vk_object = if prev_object.nil?
      in_objects.first
    else
      my_vk_object_id = prev_object_index - 1
      my_vk_object_id < 0 ? nil : in_objects[my_vk_object_id]
    end

    if my_vk_object.nil?
      return nil
    end
    if @@next_prev_id[tag] == my_vk_object.id
      return nil
    end
    @@next_prev_id[tag] = my_vk_object.id

    my_vk_object
  end
end