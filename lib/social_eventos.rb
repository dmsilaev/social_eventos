require 'eventmachine'
require 'em-http-server'
require 'social_eventos/version'
require 'hashie'
require 'pry'

module SocialEventos
  autoload :Configuration,    'social_eventos/configuration'
  autoload :Stream,    'social_eventos/stream'
  autoload :Server,    'social_eventos/server'
  autoload :ReturnStrategy,    'social_eventos/return_strategy'

  autoload :Client, 'social_eventos/client'
  autoload :Sender, 'social_eventos/sender'

  Dir[File.dirname(__FILE__) + '/social_eventos/clients/*.rb'].each do |f|

    filename = File.basename(f, '.rb')
    client_name = filename + '_client'

    client_class = client_name.split('_').map(&:capitalize).join

    autoload client_class, f
  end

  Dir[File.dirname(__FILE__) + '/social_eventos/return_strategies/*.rb'].each do |f|

    filename = File.basename(f, '.rb')
    client_name = filename + '_return_strategy'

    client_class = client_name.split('_').map(&:capitalize).join

    autoload client_class, f
  end

  @configuration ||= Configuration.new

  class << self
    attr_accessor :configuration

    def configure
      yield(configuration)
    end
  end
end
